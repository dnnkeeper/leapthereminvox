# README #

ThereminVox simulator that detects physics colliders inside theremin volume, measures average distance and generates sound. Supports unity audio effects and vibrato.

https://www.youtube.com/watch?v=JhML-CZoUf0