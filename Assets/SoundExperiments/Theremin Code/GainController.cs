﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GainController : MonoBehaviour
{
    public WaveGenerator waveGenerator;

    public float mod = 2f;

    public float maxVolume = 0.1f;
    public float minVolume = 0f;

    public float nearestControllerDistance;
    public Transform nearestT;

    public List<Transform> controlsList;

    // Use this for initialization
    void Start()
    {
        //sinus = GetComponent<Sinus>();
    }
    
    // Update is called once per frame
    void Update()
    {
        int activeControls = 0;
        
        nearestControllerDistance = Mathf.Infinity;
        nearestT = transform;

        foreach (Transform t in controlsList)
        {
            if (!t.gameObject.activeInHierarchy)
                continue;

            activeControls++;

            float d = Vector3.Project(t.position - transform.position, transform.up).magnitude;
            if (d < nearestControllerDistance)
            {
                nearestT = t;
                nearestControllerDistance = d;
            }

        }
            
        if (activeControls > 0)
        {
            waveGenerator.gain = Mathf.Clamp(nearestControllerDistance - 0.01f, 0, 1f / mod) * mod;
                //Mathf.Lerp((float)sinus.gain, Mathf.Lerp(minVolume, maxVolume, Mathf.Clamp(nearestControllerDistance - 0.01f, 0, 1f / mod) * mod), 10f*Time.deltaTime);
        }
        else
        {
            waveGenerator.gain = Mathf.Lerp((float)waveGenerator.gain, 0f, Time.deltaTime);
        }
        Debug.DrawLine(transform.position, transform.position + transform.up * (float)waveGenerator.gain, Color.yellow);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!controlsList.Contains(other.transform))
        {
            controlsList.Add(other.transform);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (controlsList.Contains(other.transform))
        {
            controlsList.Remove(other.transform);
        }
    }
}
