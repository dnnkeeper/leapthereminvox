﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(Sinus))]
[RequireComponent(typeof(LineRenderer))]
public class FrequencyController : MonoBehaviour
{
    public float power;

    public WaveGenerator waveGenerator;

    public Transform stickTransform;

    public float radius = 0.1f;

    public float step = 0.1f;

    public int lastOctave = 3;

    public int firstOctave = 1;
    
    public float freqDistanceMod = 2f;

    public float controllerDistance;
    public Transform nearestT;

    public float farthestDistance;
    
    public List<Transform> controlsList;

    public int lightningSegments = 5;

    public float lightningOffset = 0.25f;

    public Color lightingColor;

    LineRenderer lineRenderer;

    // Use this for initialization
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }
    
    // Precise method, which guarantees v = v1 when t = 1.
    double LerpDouble(double v0, double v1, double t)
    {
        return (1.0 - t) * v0 + t * v1;
    }

    public Vector3 nearestCollisionPoint;

    // Update is called once per frame
    void Update()
    {
        //step = 1f / ((lastOctave - firstOctave) * 7 * freqDistanceMod);
        DrawNotesWithStep();

        int activeControls = 0;

        if (controlsList.Count > 0)
        {

            controllerDistance = 0f;

            nearestCollisionPoint = stickTransform.position;

            float nearestDistance = Mathf.Infinity;
            
            

            foreach (Transform t in controlsList)
            {
                if (!t.gameObject.activeInHierarchy)
                    continue;

                activeControls++;
                
                Vector3 stickProjectedPoint = stickTransform.position + Vector3.Project(t.position - stickTransform.position, stickTransform.up); //Vector3.ProjectOnPlane(t.position- stickTransform.position, stickTransform.right);

                Vector3 closestPoint = t.GetComponent<Collider>().ClosestPoint(stickProjectedPoint);

                float d = (closestPoint - stickProjectedPoint).magnitude;

                controllerDistance += d;
                
                if (d < nearestDistance)
                {
                    nearestDistance = d;

                    nearestCollisionPoint = closestPoint;
                }

                //lineRenderer.positionCount = activeControls + 1;

                //lineRenderer.SetPosition(activeControls, closestPoint);

                //Debug.DrawLine(stickTransform.position, stickProjectedPoint, Color.cyan);

                Debug.DrawLine(stickProjectedPoint, closestPoint);
            }
            controllerDistance /= activeControls;
            controllerDistance *= freqDistanceMod;
            controllerDistance = step > 0 ? controllerDistance - (controllerDistance % step) : controllerDistance;

            if (activeControls > 0)
            {
                //Vector3 projectedPos = stickTransform.position + Vector3.ProjectOnPlane(nearestCollisionPoint - stickTransform.position, transform.up);
                Vector3 projectedPos = stickTransform.position + Vector3.Project(nearestCollisionPoint - stickTransform.position, stickTransform.right);
                Debug.DrawLine(stickTransform.position, projectedPos, Color.yellow);
                Debug.DrawLine(projectedPos, nearestCollisionPoint, Color.yellow);

                power = Mathf.Lerp(lastOctave, firstOctave, controllerDistance);

                float targetFrequency = 55f * Mathf.Pow(2, power);

                waveGenerator.frequency = targetFrequency;//LerpDouble(sinus.frequency, targetFrequency, Time.deltaTime * 60f);
            

            }
            
        }

        if(activeControls == 0 || controlsList.Count == 0)
        {
            //ClearLineRenderer();
        }
    }

    void ClearLineRenderer()
    {
        lineRenderer.SetPositions(new Vector3[] { });
        lineRenderer.positionCount = 0;
    }

    public float noiseAmount = 0.1f;

    void DrawNotesWithStep()
    {
        
        int octavesCount = (lastOctave - firstOctave);
        float fStep = 0.08333333333333333333333333333333f / (octavesCount * freqDistanceMod);
        Vector3 vStep = stickTransform.up * stickTransform.lossyScale.y * 0.05f;
        List<Vector3> points = new List<Vector3>();
        //AnimationCurve waveForm = sinus.WaveForm;
        for (int octaveN=0; octaveN < octavesCount; octaveN++)
        {
            for (int n = 0; n < 12; n++)
            {
                Vector3 upperPoint = stickTransform.position + stickTransform.right * fStep * (octaveN * 12 + n) + stickTransform.up * stickTransform.lossyScale.y;
                points.Add(upperPoint);
                
                for (int d = 0; d < 40; d++)
                {
                    Vector3 midPoint = upperPoint - vStep * d;
                    //points.Add(midPoint + stickTransform.right * Random.Range(-1f, 1f) * noiseAmount * fStep * Mathf.Clamp01( 1f - ( Vector3.Project(nearestCollisionPoint, stickTransform.right)- Vector3.Project(midPoint, stickTransform.right)).magnitude/fStep)* sinus.gain );
                    points.Add(midPoint + stickTransform.right * (Mathf.Sin((float)(Time.time+d*0.01f)*(float)waveGenerator.frequency * 0.1f)) * noiseAmount * fStep * Mathf.Clamp01(1f - (Vector3.Project(nearestCollisionPoint, stickTransform.right) - Vector3.Project(midPoint, stickTransform.right)).magnitude / fStep) * waveGenerator.gain);
                    //float noteFreq = 55f * Mathf.Pow(2, ((octaveN * 12) + n) * Mathf.Pow(2,1f/12f)/2f);
                    //points.Add(midPoint + stickTransform.right * (Mathf.Sin((float)(Time.time + d) * noteFreq)) * noiseAmount * fStep * Mathf.Clamp01(1f - (Vector3.Project(nearestCollisionPoint, stickTransform.right) - Vector3.Project(midPoint, stickTransform.right)).magnitude / fStep) * sinus.gain);
                }
                
                points.Add(stickTransform.position + stickTransform.right * fStep * (octaveN*12 + n) - stickTransform.up * stickTransform.lossyScale.y);
                points.Add(stickTransform.position + stickTransform.right * fStep * (octaveN * 12 + n) + stickTransform.up * stickTransform.lossyScale.y);
            }
        }

        points.Add(stickTransform.position + stickTransform.right * fStep * (octavesCount * 12) + stickTransform.up * stickTransform.lossyScale.y);
        points.Add(stickTransform.position + stickTransform.right * fStep * (octavesCount * 12) - stickTransform.up * stickTransform.lossyScale.y);

        lineRenderer.positionCount = points.Count;
        lineRenderer.SetPositions(points.ToArray());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!controlsList.Contains(other.transform))
        {
            controlsList.Add(other.transform);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (controlsList.Contains(other.transform))
        {
            controlsList.Remove(other.transform);
        }
    }

    private void OnDrawGizmos()
    {
        if (stickTransform != null) {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(stickTransform.position, 1f/freqDistanceMod);
        }
    }
}
