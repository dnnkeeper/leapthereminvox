﻿using UnityEngine;
using System;  // Needed for Math

[RequireComponent(typeof(AudioSource))]
public class WaveGenerator : MonoBehaviour
{
    // un-optimized version
    public double frequency = 440;
    public double real_frequency = 440;
    public float gain = 0.1f;

    private double increment;
    private double phase;
    private double sampling_frequency = 48000;


    public AnimationCurve WaveForm = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(Mathf.PI, 1f));


    public double vibratoAmount = 0.04f;
    public double vibratoFreq = 40.0f;

    double vibrato;

    float vol;

    private void LateUpdate()
    {
        //frequency = 1000f;

        vibrato = 1f + Mathf.Sin(Time.time * (float)vibratoFreq) * vibratoAmount;
        real_frequency = (frequency * vibrato);
        
    }

    void OnAudioFilterRead(float[] data, int channels)
    {
        // update increment in case frequency has changed
        increment = real_frequency * 2 * Math.PI / sampling_frequency;

        for (var i = 0; i < data.Length; i = i + channels)
        {
            phase = phase + increment;
            // this is where we copy audio data to make them “available” to Unity
            //data[i] = (float)(gain * Math.Sin(phase) );
            data[i] = (vol * WaveForm.Evaluate((float)phase));
            // if we have stereo, we copy the mono data to each channel
            if (channels == 2) data[i + 1] = data[i];
            if (phase > 2 * Math.PI)
            {
                vol = gain;
                phase = 0;
            }
        }
    }
}

