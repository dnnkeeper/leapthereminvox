﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionPlay : MonoBehaviour {
    
    public float threshold = 1f;

    public float minVolume = 0.1f;

    public float maxVolume = 1f;

    public float mod = 0.1f;

    public NoteFader note;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {

        float impulseMagnitude = collision.impulse.magnitude;
        if (impulseMagnitude >= threshold)
        {
            GameObject sound = GameObject.Instantiate(note.gameObject, collision.contacts[0].point, Quaternion.identity);
            AudioSource source = sound.GetComponent<AudioSource>();
            source.volume *= Mathf.Lerp(minVolume, maxVolume, Mathf.Clamp01(impulseMagnitude * mod) );
            source.pitch = source.pitch * Mathf.Clamp01(impulseMagnitude * mod);
            source.pitch = source.pitch - (source.pitch % 0.1428571428571429f);
            sound.SetActive(true);
            //Debug.Log(impulseMagnitude);
        }
    }
}
