﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionGlow : MonoBehaviour
{
    public float threshold = 1f;

    public float glowIntensity = 15f;

    float defaultGlowIntensity;

    float glowAmount;

    Renderer rend;

    Material mat;

    // Use this for initialization
    void Start()
    {
        rend = GetComponent<Renderer>();
        mat = rend.material;
        defaultColor = mat.GetColor("_EmissionColor");

        defaultColor = new Color( Mathf.Clamp01(defaultColor.r), Mathf.Clamp01(defaultColor.g), Mathf.Clamp01(defaultColor.b), Mathf.Clamp01(defaultColor.a) );

        defaultGlowIntensity = glowIntensity;
    }

    Color defaultColor;

    // Update is called once per frame
    void Update()
    {
        glowIntensity = Mathf.Lerp(glowIntensity, defaultGlowIntensity, Time.deltaTime * 4f);
        glowAmount = glowIntensity;//Mathf.Lerp(glowAmount, glowIntensity, Time.deltaTime);
        DynamicGI.SetEmissive(rend, defaultColor * glowAmount);
        mat.SetColor("_EmissionColor", defaultColor * glowAmount);
    }

    private void OnCollisionEnter(Collision collision)
    {
        float impulseMagnitude = collision.impulse.magnitude;
        if (impulseMagnitude >= threshold)
        {
            glowIntensity = glowIntensity + Mathf.Clamp(impulseMagnitude, 0f, 3f);
        }
    }
}
