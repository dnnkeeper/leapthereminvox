﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PianoManager : MonoBehaviour {

    public void PlayNote(GameObject go)
    {
        //go.SetActive(true);

        GameObject note = GameObject.Instantiate(go, go.transform.parent);
        note.SetActive(true);

        //go.SendMessage("PlayNote");
        
        //StartCoroutine(fadeNote(go));
    }

    public void StopPlayNote(GameObject go)
    {
        //go.SetActive(false);
    }

    IEnumerator fadeNote(GameObject go)
    {
        AudioSource source = go.GetComponent<AudioSource>();

        source.volume = 1f;

        float initialVolume = source.volume;

        while (source.volume > 0f)
        {
            source.volume -= Time.deltaTime;
            yield return null;
        }
        go.SetActive(false);
        source.volume = initialVolume;
    }
}
