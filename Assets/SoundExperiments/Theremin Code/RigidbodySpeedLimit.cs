﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class RigidbodySpeedLimit : MonoBehaviour {

    public float maxVelocityMagnitude = 10f;

    Rigidbody rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxVelocityMagnitude);
	}
}
