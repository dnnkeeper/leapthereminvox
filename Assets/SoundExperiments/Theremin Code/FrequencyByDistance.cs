﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrequencyByDistance : MonoBehaviour {

    public WaveGenerator waveGenerator;

    public Transform targetTransform;

    public float maxFreq = 2000f;
    public float minFreq = 100f;

    public float mod = 1f;

	// Update is called once per frame
	void Update () {
        waveGenerator.frequency = Mathf.Lerp(maxFreq, minFreq, (targetTransform.position - transform.position).magnitude * mod);
        //frequencySound.gain = transform.localPosition.z * 0.01f;
    }
}
