﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteFader : MonoBehaviour {

    public float fadeTime = 1f;

    float initialVolume;
    AudioSource source;
    
    private void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        initialVolume = source.volume;
        //source.PlayOneShot(source.clip);
    }

    // Update is called once per frame
    void Update () {

        if (source.volume > 0f)
        {
            source.volume -= (Time.deltaTime / fadeTime) * initialVolume; //Mathf.Lerp(source.volume, 0f, Time.deltaTime/fadeTime );
        }
        else
        {
            gameObject.SetActive(false);

            source.volume = initialVolume;

            Destroy(gameObject);
        }
    }
}
